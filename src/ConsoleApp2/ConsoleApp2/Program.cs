﻿using System;
using System.Collections.Generic;

namespace P4a
{
    class Program
    {
        
        static void Main(string[] args)
        {
            String adresseDataAdd = "C:\\Users\\feuxi\\Documents\\dataAdd.txt";
            String adresseDataCopy = "C:\\Users\\feuxi\\Documents\\dataCopy.txt";
            String adresseDataContain = "C:\\Users\\feuxi\\Documents\\dataContain.txt";
            Console.WriteLine("Taille maximale :");
            string var = Console.ReadLine();
            int ttab = int.Parse(var);
            Console.WriteLine("Pas :");
            var = Console.ReadLine();
            int pas = int.Parse(var);
            Console.WriteLine("Debut :");
            var = Console.ReadLine();
            int debut = int.Parse(var);
            string[] dataAdd = new string[ttab / pas * 3+1];
            string[] dataCopy = new string[ttab / pas * 3 + 1];
            string[] dataContain = new string[ttab / pas * 3 + 1];
            Console.WriteLine("Paramètres :: Taille maximum :" + ttab + " Pas :" + pas + " Debut :" + debut);
            dataAdd[0] = "taille_tab;Structure;temps_dexec";
            dataCopy[0] = "taille_tab;Structure;temps_dexec";
            dataContain[0] = "taille_tab;Structure;temps_dexec";
            Console.WriteLine("debut du traitement");

            string stuc = "";
            string ope = "";
            DateTime start = DateTime.Now; 
            TimeSpan dur = DateTime.Now - start;
            int posAdd = 1;
            int posCopy = 1;
            int posContain = 1;

            for (int i = pas + debut; i<=ttab; i = i + pas)
            {
                Console.WriteLine(i);
                for (int j = 0; j<3; j++)
                {
                    if (j== 0)
                    {
                        stuc = "list";
                        Console.WriteLine("  list");
                    }
                    else if (j == 1)
                    {
                        stuc = "linked";
                        Console.WriteLine("  linked");
                    }
                    else if (j==2)
                    {
                        stuc = "hash";
                        Console.WriteLine("  hash");
                    }
                    for(int k = 0; k<3; k++)
                    {
                        if (k == 0)
                        {
                            ope = "add";
                            Console.WriteLine("     add");
                        }
                        else if (k == 1){
                            ope = "copy";
                            Console.WriteLine("     copy");
                        }
                        else if(k==2)
                        {
                            ope = "contain";
                            Console.WriteLine("     contain");
                        }
                        start = DateTime.Now;
                        StructRecherche.Struc(stuc, ope, ttab);
                        dur = DateTime.Now - start;

                        if (k == 0)
                        {
                            dataAdd[posAdd] = i + ";";
                            dataAdd[posAdd] += stuc + ";";
                            dataAdd[posAdd] += dur;
                            posAdd++;
                        }
                        else if (k == 1)
                        {
                            dataCopy[posCopy] = i + ";";
                            dataCopy[posCopy] += stuc + ";";
                            dataCopy[posCopy] += dur;
                            posCopy++;
                        }
                        else if (k == 2)
                        {
                            dataContain[posContain] = i + ";";
                            dataContain[posContain] += stuc + ";";
                            dataContain[posContain] += dur;
                            posContain++;
                        }

                    }
                }
            }
            System.IO.File.WriteAllLines(adresseDataAdd, dataAdd);
            System.IO.File.WriteAllLines(adresseDataCopy, dataCopy);
            System.IO.File.WriteAllLines(adresseDataContain, dataContain);
            Console.WriteLine("Fin du traitement");
            Console.Read();
        }
    }

    class StructRecherche
    {
        public static void Struc(string structure, string operation, int taille)
        {
            ICollection<int> Struct; //Définit des méthodes pour manipuler des collections génériques : les génériques permettent de créer des méthodes ou des classes qui sont indépendantes d'un type

            switch (structure)
            {
                case "list":
                    Struct = new List<int>();
                    break;
                case "linked":
                    Struct = new LinkedList<int>();
                    break;
                case "hash":
                    Struct = new HashSet<int>();
                    break;
                default:
                    Console.WriteLine("Invalid, default to LinkedList");
                    Struct = new LinkedList<int>();
                    break;
            }

            Random rdn = new Random();

            switch (operation)
            {
                case "add":
                    remplir(Struct);
                    break;
                case "copy":
                    int[] copie = new int[taille];
                    remplir(Struct);
                    copier(Struct, copie);

                    break;
                case "contain":
                    int contient = rdn.Next();
                    remplir(Struct);
                    contain(Struct, contient);
                    break;
                default:
                    Console.WriteLine("Invalid");
                    break;
            }

            void remplir(ICollection<int> var)
            {
                for (int i = 0; i < taille; i++)
                {
                    var.Add(rdn.Next());
                }
            }

            void copier(ICollection<int> aCopier, int[] copie)
            {
                aCopier.CopyTo(copie, 0);
            }

            void contain(ICollection<int> contenant, int contient)
            {
                bool test = contenant.Contains(contient);
            }
        }
    }
}
