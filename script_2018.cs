using System.Collections;
using System.Collections.Generic;
using System;

class Program
{

    static void Main(string[] args)
    {
        ICollection<int> Struct; //Définit des méthodes pour manipuler des collections génériques : les génériques permettent de créer des méthodes ou des classes qui sont indépendantes d'un type

        string structure = "list";
        string operation = "access";
        int taille = 1000;

        switch (structure)
        {
            case "list":
                Struct = new List<int>();
                break;
            case "linked":
                Struct = new LinkedList<int>();
                break;
            default:
                Console.WriteLine("Invalid, default to LinkedList");
                Struct = new LinkedList<int>();
                break;
        }

        Random rdn = new Random();

        switch (operation)
        {
            case "add":
                remplir(Struct);
                break;
            case "access":
                remplir(Struct);
                foreach (object o in Struct)
                {
                    Console.WriteLine(o);
                }
                break;
            default:
                Console.WriteLine("Invalid");
                break;
        }

        void remplir(ICollection<int> var)
        {
            for (int i = 0; i < taille; i++)
            {
                var.Add(rdn.Next());
            }
        }
    }
}
