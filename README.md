
# P4a : Analyse de performances de différentes structures

[Grille d'évaluation P4a](https://docs.google.com/spreadsheets/d/1x72glVEQHPx56Wr8G0RNQgfQXGX6xCsjms_6b7J6si0/edit?usp=sharing
)

## Problème

Description du Problème.
On va s'interesser ici aux différence lors du traitement des listes classique, des listes chaînées et des liste de hashage.
Touts ces objet ont en C# la particularité d'être du type `ICollection<int>`.

Extraits de la documentation C# :

[List<T> Class](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.ilist-1?view=netframework-4.7.2)
```
List<T> Class
Represents a strongly typed list of objects that can be accessed by index. Provides methods to search, sort, and manipulate lists.
```

[LinkedList<T> Class](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.linkedlist-1?view=netframework-4.7.2)
```
LinkedList<T> Class
Represents a doubly linked list.
```

[LinkedListNode<T> Class](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.linkedlistnode-1?view=netframework-4.7.2)

```
LinkedListNode<T> Class
Represents a node in a LinkedList<T>. This class cannot be inherited.
```

[HashSet<T> Class](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.hashset-1?view=netframework-4.7.2)
```
HashSet<T> Class
Represents a set of values.
```
 Sur ces listes nous ferront deux types d'opérations : de la recherche d'élément et de la copie des élément dans un tableau. Au vu des structures utilisées, on s'attend a avoir de la part des listes une complexité linéaire et de la part des deux autres type une complexité exponentielle.


## Dispositif expérimental

### Application

[code source de l'application](src/ConsoleApp2/ConsoleApp2/Program.cs)

Lors de l'execution de l'application, on obtient trois tableaux, un pour chaque opérations sur les données. Ces tableaux sont stockés sous la forme de fichiers textes avec pour entêtes : 

```
taille_tab;Structure;temps_dexec
```
`taille_tab` correspond à la taille de la structure, `Structure` correspond à la structure, `temps_dexec` au temps d'execution.

L'application demande trois paramètres pour s'executer : la `taille maximum`, le `pas` et la `position de départ`.

### Environnement de test

Les test se font sur mon PC, un HP pavillon.
```
Processeur : Intel® Core™ i7-4500U CPU @ 2.40GHz × 8 64 Bits
RAM : 7,88 Go
```

### Description de la démarche systématique

Pour tester toute les structure de données, on lance le programme et on choisi de préférence une valeur maximale élevée afin de réduire le bruit. On divise cette valeur par 20 environ pour avoir le pas. Pour la position de départ, on peut prendre un valeur de préférence differente d'une puissance de 2 afin d'éviter d'avoir des données érronnées.

Il suffit donc d'executer le programme et de taper la valeur maximum, le pas et la position de départ dans la console.
``` 
Taille maximale :
1000000
Pas :
50000
Debut :
13
Paramètres :: Taille maximum :1000000 Pas :50000 Debut :13
debut du traitement
50013
  list
     add
     copy
     contain
  linked
     add
     ...
```

## Résultats préalables

### Temps d'exécution

| Opération            | Liste, Liste chaînée, Liste Hashée              
|----------------------|-----------------------------------
| Contient             | ![plot](images/res1.png) |
| Copie                | ![plot](images/res2.png) | 


### Consommation mémoire


### Analyse des résultats préalables

- La liste `List<>` possède un complexité qui a l'air linéaire. Elle est aussi bien plus efficace que les deux autres listes. 
- La liste chainée `LinkedList<>` est la moins efficace et possède une complexité d'apparance quadratique si ce n'est exponetielle. 
- La liste d'élements haché `HashSet` semble suivre le même pattern que la liste chainée (complexité quadratique voir exponentielle) mais est plus efficace.

### Discussion des résultats préalables

Les résultats corresponde plutôt a ce a quoi on s'attendais mais nous ne nous attendions pas a une si grande différence entre la liste classique et les autres listes. Il faudrait peut-être avoir plus de résultat pour s'assurer que la complexité de la liste classique est linéaire et surtout affiner la complexité des deux autres structures. Avoir des résultats moins étendus pourrais nous aider.

## Etude approfondie

### Hypothèse
Avec plus de données à traiter, on pourra déterminer les compléxités exactes des listes chaînées et des listes de hash. On pourra de plus confirmer la complexité des listes classiques. 

### Protocole expérimental de vérification de l'hypothèse

Le protocole correspond à la multiplication par dix de la taille des structures de données.

```
Taille maximale :
10000000
Pas :
500000
Debut :
51
Paramètres :: Taille maximum :10000000 Pas :500000 Debut :51
debut de traitement
500051
   list
      add
      copy
      contain
   linked
      add
      copy
      contain
   hash
      add
      copy
      contain
   ...
```

### Résultats expérimentaux
| Opération            | Liste, Liste chaînée, Liste Hashée              
|----------------------|-----------------------------------
| Contient               | ![plot](images/res21.png) |
| Copie                  | ![plot](images/res22.png) | 

### Analyse des résultats expérimentaux


Dans ce cas d'analyse, nous analysons un plus grand nombre de données, des tableaux plus grands. Dans l'opération contain, on constate que l'écart entre la structure hash et structure linkedList a diminué. Ils ont tendance à s'aligner beaucoup plus. Cependant, le hash reste un peu plus performant que le linkedList. Plus encore, le bruit reste assez fort et les données dispersées, surtout quand il s'agit des linkedList. Les listes simples restent quand à elles assez stable. S'agissant de l'opération de copie, l'alignement entre hash et linkedList est moins marqué. Cette fois c'est le linkedList qui est plus perfomant. La dispersion est aussi moins marquée. Les listes simples ont un résultat similaire.

### Discussion des résultats expérimentaux

Les résultats sont généralement décevants :
* L'analyse des résultats des listes classiques confirmes notre hypothèse où la complexité est linéaire.
* L'analyse des résultats des listes chaînées et hashées se révèlent bien trop irrégulières car les données sont trop dispersées et ne correspondent en plus pas à nos hypothèses de complexité.

## Conclusion et travaux futurs

Cette étude se révèle être relativement un échec. En effet, seul le tiers de nos hypothèses ont pu être prouvées. Pour la suite il faudra donc se pencher sur le cas des listes chaînées et hashées en essayant sur plusieurs configurations de machine pour gagner en stabilité, augmenter encore le nombre de données car nous avons constaté une stabilisation, il est encourageant de penser que avec l'augmentation des données, elle s'affinera. 